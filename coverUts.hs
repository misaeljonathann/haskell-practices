data Expr = C Float | Expr :+ Expr
        | Expr :- Expr | Expr :* Expr
        | Expr :/ Expr | V String
        | Let String Expr Expr

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)      = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c)         = (C c)
subst v0 e0 (e1 :+ e2)  = subst v0 e0 e1 :+ subst v0 e0 e2
-- e1.pastiBisa == (e1 :+ e2)
--
-- v0 = "x"
-- e0 = (C 5)
-- e1 = V "x"
-- e2 = (C 7)
-- subst x (C 5) V "x"
subst v0 e0 (e1 :- e2)  = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)  = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)  = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2)  = Let v0 e0 (subst v1 e1 e2)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2)     = evaluate e1 + evaluate e2
evaluate (e1 :- e2)     = evaluate e1 - evaluate e2
evaluate (e1 :* e2)     = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)     = evaluate e1 / evaluate e2
evaluate (Let v e0 e1)  = evaluate (subst v e0 e1)
evaluate (V _)          = 0.0

-- Mencoba eksplorasi bagian yang "mungkin perlu dikoreksi"
pastiBisa = evaluate (Let "x" (C 5) (V "x" :+ (C 7)))
-- Let v expr1 expr2
-- v = "x"
-- expr1 = (C 5)
-- expr2 = (V "x" :+ (C 7))
bisaGa = evaluate (Let "x" (C 5) (Let "y" (V "x") (V "y" :* V "x")))

-- bisaGa ini bakal bisa dievaluasi kalau notasi substitusinya
-- subst v0 e0 (Let v1 e1 e2)  = Let v0 e0 (subst v1 e1 e2)
-- sedangkan yang diberikan soal
-- subst v0 e0 (Let v1 e1 e2)  = Let v1 e1 (subst v0 e0 e2)
-- #urutan evaluasi yang tertukar

-- Tahap : #Lom tentu bener
-- evaluate (subst "x" (C 5) (Let "y" (C 10) (V "y" :/ V "x")))
-- evaluate (Let "y" (C 10) (subst "x" (C 5) (V "y" :/ V "x")))
-- evaluate (Let "y" (C 10) (V "y" :/ C 5))
-- evaluate (C 10 :/ C 5)

-- perlu dikoreksi ga ya? mmm kayaknya mgkn Let v1 e1 (subst v0 e0 v2) perlu dituker
-- sama Let v0 e0 (subst v0 e0 v2), gatau, padahal hasilnya sama


--Imperative Robot Language

data Direction = North | East | South | West
        deriving (Eq, Show, Enum)

right :: Direction -> Direction
right d = toEnum (succ (fromEnum d) `mod` 4)




