import Data.List

-- -- Cool Stuff:
-- https://wiki.haskell.org/Instances_of_List_and_Maybe

-- -- Exercises from chapter 9-10
-- 1. Define length function using "map" and "sum"
toOne x = 1
getLength ls = sum (map toOne (ls))

-- variation: fold
getLength2 ls = foldr (+) 0 (map toOne (ls))

-- 2. What does map (+1) (map (+1) xs) do
what xs = map (+1) (map (+1) xs)

-- variation
what2 :: [Integer] -> Integer -> [Integer]
what2 xs n = map (*n) (map toOne (xs))

-- 3. iteration
goIter 0 _ x = x
goIter n f x = goIter (n - 1) f (f x)

-- 4. sumofSquares using map and foldr
toSquare x = x * x
toSquareMap xs = map toSquare xs
sumOfSquares n = foldr (+) 1 (toSquareMap [2..n])

-- How does the functon behave?
-- kayaknya cuma iterate getElement dan dibikin list lagi yang sama
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- 5. Flip
goFlip f x y = f y x

-- -- FROM LIST COMPREHENSION TO HOF
-- rewrite [x+1|x<-xs]
rewrite1 ls = map (+1) ls

-- rewrite [x+y|x<-xs,y<-ys]
rewrite2 xs [] = xs
rewrite2 [] xs = xs
rewrite2 (x:xs) (y:ys) = (x+y) : rewrite2 xs ys

-- variation
rewrite2var xs ys = zipWith (+) xs ys

-- variation2
rewrite2var xs ys = concat (map (\y -> concat (map (\x -> [x + y]) xs) ) ys)

-- rewrite [x+2|x<-xs,x>3]
rewrite3 [] = []
rewrite3 (x:xs) | x > 3 = (x+2) : rewrite3 xs
                | otherwise = rewrite3 xs

-- variation
rewrite3var ls = map (+2) (filter (>3) ls)

-- -- GENERATE LIST COMPREHENSION
-- toListComp : map (+3) xs
toListComp xs = [x + 3| x <- xs]

-- toListComp : filter (>7) xs
toListComp2 xs = [x | x <- xs, x > 7]

-- toListComp : concat (map (\x -> map (\y -> (x,y)) ys) xs)
-- toListComp2 (x:xs) (y:ys) = 

cobaZip = zip [1,2,3] [3,4,5]
cobaZipWith = zipWith (\x y -> zip [x] [y]) [1,2,3] [2,2,2]

-- Generate Listing
-- listOfLength :: Integer -> Gen a -> Gen [a]
-- listOfLength x y 

----------------------------

animalFriends :: [(String, String)]
animalFriends = [ ("Pony", "Lion")
                , ("Lion", "Manticore")
                , ("Unicorn", "Lepricon") ]



-- -- Tree

-- Lazy Evaluation Exercises
lazy1 = [x+y | x <- [1..4], y <- [2..4], x > y]
divisor n = [x | x <- [1..n], (n `mod` x) == 0]

quickSort [] = []
quickSort (x:xs) = quickSort [z | z <- xs, z < x]
                    ++ [x] ++ 
                    quickSort [y | y <- xs, y > x]

perms [] = [[]]
perms ls = [x:sisa | x <- ls, sisa <- perms (ls \\ [x])]

-- -- Monads Trial
animalFriendLookup :: [(String, String)] -> Maybe String
animalFriendLookup animalMap =
    lookup "Pony" animalMap
    >>= (\ponyFriend -> lookup ponyFriend animalMap)
    >>= (\pony2ndFriend -> lookup pony2ndFriend animalMap)
    >>= (\friend -> Just friend)

sugaryFriendLookup :: [(String, String)] -> Maybe String
sugaryFriendLookup animalMap = do
    ponyFriend <- lookup "Pony" animalMap
    ponyFriend1 <- lookup ponyFriend animalMap
    ponyFriend2 <- lookup ponyFriend1 animalMap
    return ponyFriend2


-- Create own monad instead of >>=
ownMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
ownMaybe Nothing f = Nothing
ownMaybe (Just x) f = f x

trial1 = Just 3 >>= (\a -> return (a+3))
trial2 = Just 4 >>= (\a -> return a >>= (\y -> return (y+2)))

trial3 = do     -- MIND BLOWN
        x <- [1,2,3]
        y <- [4,5,6]
        return (x + y)
-- abstraction for trial3

abstraction :: (a -> b -> c) -> [a] -> [b] -> [c]
abstraction f xs ys = do
                    x <- xs
                    y <- ys
                    return (f x y)
-- Contoh Penggunaan:
-- abstraction (\a b -> [a,b]) [1,2,3] [1,2,3]
-- abstraction (+) [1,2,3] [4,5,6]

monad2 = ([1,2,3] >>= (\x -> [4,5] >>= (\y -> return (x,y))))

-- Trial to brewek2 monad using monad instance knowledge
test2 = ([1,2,3] >>= \x -> [4,5] >>= \y -> return (x,y))

test3 = concat (map (\x -> [4,5] >>= \y -> return (x,y)) [1,2,3])

test4 = concat (map (\x -> (concat (map (\y -> return (x,y)) [4,5]))) [1,2,3])

test5 = concat (map (\x -> (concat (map (\y -> [(x,y)])
                                        [4,5])))
                    [1,2,3])

-- -- FUNFACT
-- tidak valid:
-- 9 >>= \x -> return (x*10)
-- Just [1,2,3] >>= \x -> return (x*10)
-- valid:
-- Just 9 >>= \x -> return (x*10)
-- [1,2,3] >>= \x -> return (x*10) #list secara tidak langsung instance maybe (?)

coba1 = map (+3) [1..5]
coba2 = fmap (+3) (5, 7) -- loh ini yang nambah cuma yg belakang
coba3 = foldr (+) 1 (Just 2)

liftM :: (Monad m) => (a -> b) -> (m a -> m b)
liftM f = \a -> do {     -- "do" di notasi lambda mesti ada ";"
                x <- a ;
                return (f x)
}

liftM2 :: (Monad m) => (a -> b -> c) -> m a -> m b -> m c
liftM2 f = \a b -> do {
                x <- a ;
                y <- b ;
                return (f x y)
}

-- Merge
merge (x:xs) (y:ys)
    | (x == y) = x : merge xs ys
    | (x < y) = x : merge xs (y:ys)
    | (x > y) = y : merge (x:xs) ys

-- Merge Sort

merges [] kanan = kanan
merges kiri [] = kiri
merges kiri@(x:xs) kanan@(y:ys) | x < y = x : merge xs kanan
                                | y <= x = y : merge kiri ys


mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = merges (mergeSort kiri) (mergeSort kanan)
        where   kiri = take ((length xs) `div` 2) xs
                kanan = drop ((length xs) `div` 2) xs

-- Ask Input
nameDo :: IO ()
nameDo = do putStr "Belajar pemfung yuk? "
            first <- getLine
            putStr "Sambil makan ga? "
            last <- getLine
            putStrLn ("Jawaban tadi : " ++ first ++ " dan " ++ last)

nameDoMonad :: IO ()
nameDoMonad = putStr "Belajar pemfung yuk? " >>
            getLine >>= \first ->
            putStr "Sambil makan ga? " >>
            getLine >>= \last ->
            putStrLn ("Jawaban tadi : " ++ first ++ " dan " ++ last)

type Sexpr = String

transformStmt :: Sexpr -> Int -> (Sexpr, Int)
transformStmt expr counter = (newExpr, counter+1)
    where   newExpr = "(define " ++ var ++ " " ++ expr ++ ")"
            var = "tmpVar" ++ (show counter)

------------------------------------------------------------------

subst :: String -> Expr -> Expr -> Expr

subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

-- data Expr = C Float | Expr :+ Expr | Expr :- Expr
--             | Expr :* Expr | Expr :/ Expr
--             | V [Char] | Let String Expr Expr

-- evaluate :: Expr -> Float

-- evaluate (C x) = x
-- evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
-- evaluate (e1 :- e2) = evaluate e1 - evaluate e2
-- evaluate (e1 :* e2) = evaluate e1 * evaluate e2
-- evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
-- evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
-- evaluate (V v)         = 0.0  

-- exp0 = (((C 5.0) :+ ((C 7) :* (C 4))))
-- exp1 = (((C 4.9) :+ (C 2.3) :/ (C 6)))
-- exp2 = Let "x" (C 7) ((C 5) :+ (C 7) :* (V "x"))



-- -- Tambahan Ide Latihan Persiapan UTS
data Expr = C Float | Expr :+ Expr | Expr :- Expr
            | Expr :* Expr | Expr :/ Expr
            | V String | Let String Expr Expr

-- versi Rekursif
evaluate :: Expr -> Float

evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V v)         = 0.0  


splits [] = [([], [])]
splits (x:xs) = ([], x:xs) : [(x:ps, qs) | (ps, qs) <- splits xs]

-- versi rancangan HOF
